package com.michalrys.notebookwithhistory;

import com.michalrys.notebookwithhistory.controller.NotebookController;
import com.michalrys.notebookwithhistory.model.Memento;
import com.michalrys.notebookwithhistory.model.Notebook;

import java.util.List;

public class RunQuickRun {
    public static void main(String[] args) throws InterruptedException {
        // Design Pattern = Memento
        NotebookController notebookController = new NotebookController(new Notebook("my_notes"));

        notebookController.setTitle("my dummy notes");

        System.out.println("\n====== do some operations:");
        notebookController.setNotes("Ola ma chomika,");
        notebookController.appendNote("\nAla ma kota");
        Thread.sleep(10);
        notebookController.clearNotes();
        Thread.sleep(10);
        notebookController.setNotes("Tom ma psa");
        Thread.sleep(10);
        notebookController.setNotes("A w Krakowie pada deszcz...");
        Thread.sleep(10);
        System.out.println(notebookController.getNotesReadyForDisplayingOnView());

        System.out.println("\n====== undo - redo check:");
        notebookController.undo();
        notebookController.undo();
        notebookController.undo();
        notebookController.undo();
        notebookController.undo();
        notebookController.undo();
        notebookController.redo();
        notebookController.redo();
        notebookController.redo();
//        notebookController.redo();
//        notebookController.redo();
        System.out.println(notebookController.getNotesReadyForDisplayingOnView());

        System.out.println("\n====== save undo/redo history:");
        notebookController.saveNotebookUndoAndRedoHistoryToAFile("some file path");

        System.out.println("\n====== undo history:");
        List<Memento> historyForUndo = notebookController.getUndoHistory();
        for (Memento undo : historyForUndo) {
            System.out.printf("%s\n", undo.getTimeStamp());
        }

        System.out.println("\n====== redo history:");
        List<Memento> redoHistory = notebookController.getRedoHistory();
        for(Memento redo: redoHistory) {
            System.out.printf("%s\n", redo.getTimeStamp());
        }
    }
}