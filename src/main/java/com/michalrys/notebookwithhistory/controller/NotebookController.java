package com.michalrys.notebookwithhistory.controller;

import com.michalrys.notebookwithhistory.model.Memento;
import com.michalrys.notebookwithhistory.model.Notebook;

import java.util.LinkedList;
import java.util.List;

public class NotebookController {
    private final Notebook notebook;
    private LinkedList<Memento> undoHistory = new LinkedList<>();
    private LinkedList<Memento> redoHistory = new LinkedList<>();

    public NotebookController(Notebook notebook) {
        this.notebook = notebook;
        undoHistory.add(notebook.saveCurrentState());
    }

    public void setTitle(String title) {
        notebook.setTitle(title.toLowerCase().replaceAll(" ", "_"));
    }

    public void setNotes(String notes) {
        notebook.setNotes(notes);
        undoHistory.add(notebook.saveCurrentState());
        redoHistory.clear();
    }

    public void appendNote(String note) {
        notebook.appendNote(note);
        undoHistory.add(notebook.saveCurrentState());
        redoHistory.clear();
    }

    public void clearNotes() {
        notebook.clearNotes();
        undoHistory.add(notebook.saveCurrentState());
        redoHistory.clear();
    }

    public String getNotesReadyForDisplayingOnView() {
        // TODO add some additional operation before notes will be send to a view
        return notebook.getNotes();
    }

    public void undo() {
        if (undoHistory.size() == 1) {
            return;
        }
        redoHistory.add(undoHistory.pollLast());
        notebook.restoreState(undoHistory.peekLast());
    }

    public void redo() {
        if (redoHistory.isEmpty()) {
            return;
        }
        notebook.restoreState(redoHistory.peekLast());
        undoHistory.add(redoHistory.pollLast());
    }

    public List<Memento> getUndoHistory() {
        return undoHistory;
    }

    public List<Memento> getRedoHistory() {
        return redoHistory;
    }

    public void saveNotebookUndoAndRedoHistoryToAFile(String filePath) {
        // TODO implement some method
        // Object keep in field notebook in this class also must be saved somehow - this is current state of Notebook.
        // serialization could be used
        System.out.println("Saving:");
        System.out.printf("\t1. Notebook \"%s\" was saved\n", notebook.getTitle());
        System.out.printf("\t2. Undo history was saved\n", notebook.getTitle());
        for (Memento memento : undoHistory) {
            System.out.printf("\t\t%s\n", memento.getTimeStamp());
        }
        System.out.println("\t3. Redo history was saved");
        for (Memento memento : redoHistory) {
            System.out.printf("\t\t%s\n", memento.getTimeStamp());
        }
    }

    public void readNotebookUndoAndRedoHistoryFromAFile(String filepath) {
        // TODO implement some how reading saved history
        // TODO read a file and set up fields: memento, undoHistory, redoHistory.
    }
}