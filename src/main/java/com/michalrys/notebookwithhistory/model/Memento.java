package com.michalrys.notebookwithhistory.model;

public interface Memento {
    String getTimeStamp();

    void restoreState();
}