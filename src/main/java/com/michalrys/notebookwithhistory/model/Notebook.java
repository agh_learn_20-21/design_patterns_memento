package com.michalrys.notebookwithhistory.model;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Notebook {
    private String title;
    private StringBuilder notesHolder = new StringBuilder();
    //FIXME: here could be Strategy <<interface>> NotesHolder <- - - PlainTextNotesHolder

    public Notebook(String title) {
        this.title = title;
    }

    private class NotebookMemento implements Memento {
        private final Notebook notebook;
        private final String title;
        private final StringBuilder notesHolder;
        private final String timeStamp;

        public NotebookMemento(Notebook notebook) {
            this.notebook = notebook;
            this.title = notebook.title;
            this.notesHolder = new StringBuilder();
            this.notesHolder.append(notebook.notesHolder.toString());
            timeStamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern("YYYY-MM-dd_HH-mm-ss-n"));
        }

        @Override
        public void restoreState() {
            notebook.setTitle(title);
            notebook.notesHolder = notesHolder;
        }

        @Override
        public String getTimeStamp() {
            return timeStamp;
        }
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void clearNotes() {
        notesHolder = new StringBuilder();
    }

    public void setNotes(String notes) {
        this.notesHolder = new StringBuilder(notes);
    }

    public void appendNote(String note) {
        this.notesHolder.append(note);
    }

    public String getNotes() {
        return notesHolder.toString();
    }

    public Memento saveCurrentState() {
        return new NotebookMemento(this);
    }

    public void restoreState(Memento memento) {
        memento.restoreState();
    }
}